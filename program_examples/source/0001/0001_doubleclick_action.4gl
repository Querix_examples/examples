##########################################################################
# Program Examples Project                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

GLOBALS 
	DEFINE type_user TYPE AS RECORD
		user_code SERIAL,
		fname NVARCHAR(16),
		lname NVARCHAR(16),
		company NVARCHAR(20),
		address1 STRING,
		address2 STRING,
		city STRING,
		state NVARCHAR(2),
		country STRING,
		zipcode NVARCHAR(5),
		email STRING,
		phone NVARCHAR(18)
	END RECORD

END GLOBALS 

MAIN
	DEFINE l_arr_rec_users DYNAMIC ARRAY OF type_user
	DEFINE l_idx SMALLINT
	
	CALL init_array() RETURNING l_arr_rec_users
	
	OPEN WINDOW w_main WITH FORM "0001/0001_doubleclick_action_main" 

	DISPLAY ARRAY l_arr_rec_users TO sr_users.* ATTRIBUTE(UNBUFFERED) 
		BEFORE DISPLAY
			CALL dialog.SetActionHidden("ACCEPT", TRUE)
			CALL dialog.SetActionHidden("CANCEL", TRUE)
			CALL fgl_setactionlabel("DOUBLECLICK", "", "")

		BEFORE ROW 
			LET l_idx = arr_curr()
			IF l_idx > 0 THEN
				DISPLAY l_arr_rec_users[l_idx].* TO sr_user_details.*
			END IF

		ON ACTION ("EDIT","DOUBLECLICK")
			IF l_idx > 0 THEN
				CALL edit_user(l_arr_rec_users[l_idx]) RETURNING l_arr_rec_users[l_idx].*
				DISPLAY l_arr_rec_users[l_idx].* TO sr_user_details.*
			ELSE
				CALL fgl_winmessage("Empty Array", "There is no records in array", "info")
			END IF
	
		ON ACTION "EXIT"
			EXIT DISPLAY
	
	END DISPLAY

END MAIN

FUNCTION edit_user(p_user)
	DEFINE p_user type_user
	
	OPEN WINDOW w_details WITH FORM "0001/0001_doubleclick_action_details" ATTRIBUTE(BORDER)
	OPTIONS INPUT WRAP

	INPUT p_user.* WITHOUT DEFAULTS FROM sr_user_details.* 
		BEFORE INPUT
			CALL dialog.SetActionHidden("CANCEL", TRUE)

		ON ACTION "EXIT"
			EXIT INPUT

	END INPUT
	CLOSE WINDOW w_details

	RETURN p_user.*
END FUNCTION

FUNCTION init_array()
	DEFINE l_arr_rec_users DYNAMIC ARRAY OF type_user
	DEFINE l_json_arr_users util.JSONArray
	DEFINE json_file TEXT
	
	LOCATE json_file IN FILE "0001/0001_users.json"
	
	LET l_json_arr_users = util.JSONArray.parse(json_file)
	CALL l_json_arr_users.toFGL(l_arr_rec_users)

	RETURN l_arr_rec_users
END FUNCTION
